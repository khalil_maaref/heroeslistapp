//
//  MenuButtonView.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit
import RxSwift

@IBDesignable
class MenuButtonView: UIView {
    
    @IBInspectable var img: UIImage = UIImage() {
        didSet{
            if iconButton == nil {
                iconButton = UIImageView()
                self.addSubview(iconButton)
                iconButton.fixConstraintsToView(self, positionType: .center, top: 10, right: nil, bottom: nil, left: nil)
                iconButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
                iconButton.widthAnchor.constraint(equalToConstant: 36).isActive = true
                iconButton.contentMode = .scaleAspectFit
            }
            iconButton.image = img
        }
    }
    
    var iconButton: UIImageView!
    
    public static var selectedIndex = Variable(-1)
    var valueObs : Observable<Int> {
        return MenuButtonView.selectedIndex.asObservable()
    }
    let bagDisposer = DisposeBag()
    
    @IBInspectable var index: Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initClicks()
        self.initObservables()
    }
    
    func loadNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func initClicks() {
           let tap = UITapGestureRecognizer(target: self, action: #selector(onItemSelected(tap:)))
           self.isUserInteractionEnabled = true
           self.addGestureRecognizer(tap)
           
       }
       
    func initObservables() {
        valueObs.subscribe(onNext: { [weak self] value in
            if self?.index ?? -1 > -1{
                if self?.index != value{
                    UIView.animate(withDuration: 0.5) {
                        self?.iconButton.alpha = 0.3
                        self?.iconButton.frame.origin.y = (self?.iconButton.frame.width ?? 0)/2 - 15
                    }
                }
                else
                {
                    UIView.animate(withDuration: 0.5) {
                        self?.iconButton.alpha = 1.0
                        //self?.contentView.transform = CGAffineTransform.init(translationX: 0, y: 0)
                        self?.iconButton.frame.origin.y = -10
                    }
                }
            }
        }).disposed(by: bagDisposer)
    }
    
    @objc func onItemSelected(tap: UITapGestureRecognizer) {
        MenuButtonView.selectedIndex.value = index
    }

}
