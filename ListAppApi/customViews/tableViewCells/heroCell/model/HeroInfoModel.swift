//
//  HeroInfoModel.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

struct HeroInfoModel {
    
    var heroModel : HeroDataModel?
    
    init(id: String = "-1", name: String = "-") {
        heroModel = HeroDataModel(id: id, name: name)
    }
}
