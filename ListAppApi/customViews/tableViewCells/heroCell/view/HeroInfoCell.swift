//
//  HeroInfoCell.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit

protocol HeroInfoCellDelegate {
    func onItemCellClick(heroModel: HeroDataModel)
}


class HeroInfoCell: UITableViewCell {

    let modelView = HeroInfoMV(id: -1)
    var delegate: HeroInfoCellDelegate?
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.initCicks()
    }
    
    func initCicks() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onMenuIconTapped(tap:)))
        self.contentView.isUserInteractionEnabled = true
        self.contentView.addGestureRecognizer(tap)
    }
    
    @objc func onMenuIconTapped(tap: UITapGestureRecognizer) {
        self.delegate?.onItemCellClick(heroModel: self.modelView.getHeroModel())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setHeroItem(item: HeroDataModel) {
        self.modelView.setHeroModel(model: item)
    }
}
