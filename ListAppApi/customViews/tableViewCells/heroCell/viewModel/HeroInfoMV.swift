//
//  HeroInfoMV.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

class HeroInfoMV {
    
    var model  = HeroInfoModel()
    
    
    init(id: Int = -1) {
    }
    
    func setHeroModel(model : HeroDataModel) {
        self.model.heroModel = model
    }
    
    func getHeroModel() -> HeroDataModel {
        return self.model.heroModel ?? HeroDataModel(id: "-1", name: "-")
    }
    
}
