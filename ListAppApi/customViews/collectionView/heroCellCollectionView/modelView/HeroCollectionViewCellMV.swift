//
//  HeroCollectionViewCellMV.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 26/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

class HeroCollectionViewCellMV {
    
    var model = HeroCollectionViewCellModel()
    
    func setHeroModel(model : HeroDataModel) {
        self.model.heroModel = model
    }
    
    func getHeroModel() -> HeroDataModel {
        return self.model.heroModel
    }
}
