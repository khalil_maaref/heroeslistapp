//
//  HeroCollectionViewCellModel.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 26/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

struct HeroCollectionViewCellModel {
    var heroModel: HeroDataModel
    
    init(id: String = "-1", name: String = "-") {
        self.heroModel = HeroDataModel(id: id, name: name)
    }
}
