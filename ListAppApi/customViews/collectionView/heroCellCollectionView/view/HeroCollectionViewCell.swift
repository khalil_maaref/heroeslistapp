//
//  HeroCollectionViewCell.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 26/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit

protocol HeroCollectionViewCellDelegate {
    func onSavedItemCellClick(heroModel: HeroDataModel)
}


class HeroCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    let modelView = HeroCollectionViewCellMV()
    
    var delegate: HeroCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.initCicks()
    }
    
    func initCicks() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onMenuIconTapped(tap:)))
        self.contentView.isUserInteractionEnabled = true
        self.contentView.addGestureRecognizer(tap)
    }
    
    func setCurrentHeroModel(model: HeroDataModel) {
        self.modelView.setHeroModel(model: model)
    }
    
    @objc func onMenuIconTapped(tap: UITapGestureRecognizer) {
        self.delegate?.onSavedItemCellClick(heroModel: self.modelView.getHeroModel())
    }

}
