//
//  ListPageMV.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

class ListPageMV {
    
    var model  = ListPageModel()
    var api =  Api()
    var mPresenter: MainPageVCPresenter?
    
    init() {
    }
    
    func setMainPresenter(mPresenter: MainPageVCPresenter?) {
        self.mPresenter = mPresenter
    }

    func loadHerosData(valueToSearch: String, completion: @escaping (()->Void) ) {
        mPresenter?.showLoading()
        self.api.getListHeroesApiByValue(valueToSearch: valueToSearch) { (list) in
            self.mPresenter?.hideLoading()
            self.setHeroesList(list: list)
            completion()
        }
    }

    func setHeroesList(list: [HeroDataModel]) {
        self.model.listHeroes = list
    }
    
    func getItemFromListByIndex(index: Int) -> HeroDataModel? {
        guard index < self.model.listHeroes.count else {
            return nil
        }
        return self.model.listHeroes[index]
    }
    
    func getListHeroes() -> [HeroDataModel] {
        return self.model.listHeroes
    }
    
    func getListDataCount() -> Int {
        return model.listHeroes.count
    }
}
