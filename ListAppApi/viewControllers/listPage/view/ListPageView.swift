//
//  ListPageView.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit
import SDWebImage

class ListPageView: UIView {

    let modelView = ListPageMV()
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var searchInput : UITextField!
    @IBOutlet weak var imgSearch : UIImageView!
    
    var mPresenter : MainPageVCPresenter?
    var localData = LocalData()
    
    var searchInputDelayTimer : Timer?
    var isApiOnCall = false
    var tmpSearchValue = ""
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    func loadNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func initPageData() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.modelView.setMainPresenter(mPresenter: mPresenter)
        
        self.tableView.register(UINib(nibName: "HeroInfoCell", bundle: nil), forCellReuseIdentifier: "HeroInfoCell")
        
        self.modelView.loadHerosData(valueToSearch: "a") { () in // Api need to have at Least One Letter for search
            self.tableView.reloadData()
        }
        
        self.initSearchInputObserverChanges()
    }
    
    func initSearchInputObserverChanges() {
        self.searchInput.delegate = self
    }
}

extension ListPageView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.modelView.getListDataCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = self.tableView.dequeueReusableCell(withIdentifier: "HeroInfoCell", for: indexPath) as? HeroInfoCell
        
        let item = self.modelView.getItemFromListByIndex(index: indexPath.row)
        guard item != nil else {
            return cell ?? HeroInfoCell()
        }
        cell?.delegate = self
        cell?.setHeroItem(item: item ?? HeroDataModel(id: "-1", name: "-"))
        cell?.labelName.text = item?.name ?? "-"
        cell?.labelDescription.text = item?.biography?.publisher ?? "-"
        cell?.img.sd_setImage(with: URL(string: item?.image?.url ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"), options: .allowInvalidSSLCertificates, completed: nil)
        return cell ?? HeroInfoCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension ListPageView: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (self.searchInputDelayTimer != nil) {
            self.searchInputDelayTimer?.invalidate()
            self.searchInputDelayTimer = nil
        }
        searchInputDelayTimer = Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(searchForValue(_:)), userInfo: "", repeats: false)
        return true
    }
    
    @objc func searchForValue(_ timer: Timer) {
        let value = self.searchInput.text ?? ""
        self.mPresenter?.showLoading()
        self.modelView.loadHerosData(valueToSearch: value) {
            self.mPresenter?.hideLoading()
            self.tableView.reloadData()
        }
    }
}

extension ListPageView : HeroInfoCellDelegate {
    
    func onItemCellClick(heroModel: HeroDataModel) {
        self.localData.appendItemHeroToViewedHeroes(item: heroModel) // Add Viewed Hero To saved List
        self.localData.setSelectedHeroItem(hero: heroModel) // Set current Hero Selected
        MenuButtonView.selectedIndex.value = 2 // Redirec to Tab 2 -> InfoPage
    }
}
