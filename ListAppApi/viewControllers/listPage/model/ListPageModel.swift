//
//  ListPageModel.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

struct ListPageModel {
        
    var listHeroes : [HeroDataModel] = []
    
}
