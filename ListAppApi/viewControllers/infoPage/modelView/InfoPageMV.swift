//
//  InfoPageMV.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

class InfoPageMV{
    
    var model = InfoPageModel()
    
    func setCurrentHeroToModel(model: HeroDataModel) {
        self.model.selectedHero = model
    }
    
    func getCurrentHeroSelected() -> HeroDataModel {
        return self.model.selectedHero
    }
    
    func isValidHeroSaved() -> Bool {
        if self.model.selectedHero.id?.contains("-1") ?? true {
            return false
        }
        return true
    }
}
