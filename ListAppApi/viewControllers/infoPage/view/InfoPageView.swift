//
//  InfoPageView.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit
import SDWebImage

class InfoPageView: UIView {
    
    let modelView =  InfoPageMV()
    let localData = LocalData()
    
    @IBOutlet weak var lbNotFound : UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func initPageData() {
        self.loadSavedCurrentHero()
        guard self.modelView.isValidHeroSaved() else {
            self.lbNotFound.isHidden = false
            return
        }
        self.fillPageData()
    }
    
    func fillPageData() {
        self.lbNotFound.text = self.modelView.getCurrentHeroSelected().name
        self.img.sd_setImage(with: URL(string: self.modelView.getCurrentHeroSelected().image?.url ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"), options: .allowInvalidSSLCertificates, completed: nil)
        print("model = ", self.modelView.getCurrentHeroSelected())
    }
    
    func loadSavedCurrentHero() {
        let model = self.localData.getSelectedHeroItem()
        self.modelView.setCurrentHeroToModel(model: model)
    }

}

