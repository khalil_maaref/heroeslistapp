//
//  MainPage.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit
import RxSwift

protocol MainPageVCPresenter {
    func showLoading()
    func hideLoading()
}

class MainPageVC: BaseVC {

    @IBOutlet weak var contentView : UIView!
    
    var menuButtonObs : Observable<Int>{
        return MenuButtonView.selectedIndex.asObservable()
    }
    
    let bagDisposer = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initObservables()
        self.manuallySelectTab(index: 2) // init App to Tab 2
    }
    
    func initObservables() {
        menuButtonObs.subscribe(onNext: { [weak self] value in
            self?.switchPageById(id: value)
        }).disposed(by: bagDisposer)
    }
    
    func manuallySelectTab(index: Int) {
        MenuButtonView.selectedIndex.value = index
    }
    
    func switchPageById(id: Int) {
        contentView.clearSubViews()
        switch id {
        case 1:
            let listPage = ListPageView().loadNib() as! ListPageView
            contentView.addSubview(listPage)
            listPage.fixInView(contentView)
            listPage.mPresenter = self
            listPage.initPageData()
            print("load Page List")
        case 2:
            let infoPage = InfoPageView().loadNib() as! InfoPageView
            contentView.addSubview(infoPage)
            infoPage.fixInView(contentView)
            infoPage.initPageData()
            print("load Page Info")
        case 3:
            let savedPage = SavedPageView().loadNib() as! SavedPageView
            contentView.addSubview(savedPage)
            savedPage.fixInView(contentView)
            savedPage.initPageData()
            print("load Page Saved")
        default:
            print("Undefined page Index")
        }
        
    }
}

extension MainPageVC: MainPageVCPresenter{
    func showLoading() {
        self.showHudLoading(hideKey : false) // don't hide keyboard on Loading
    }
    
    func hideLoading() {
        self.hideHudLoading()
    }
    
    
}
