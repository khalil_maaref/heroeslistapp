//
//  SavedPageMV.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

class SavedPageMV {
    
    var model =  SavedPageModel()
    let localData = LocalData()

    
    func loadListHeroesLocally(){
        let arr = localData.getListViwedHeroes()
        self.setListHeroes(list: arr)
    }
    
    func setListHeroes(list: [HeroDataModel]) {
        self.model.listHeros = list
    }
    
    func getHeroAtIndex(index: Int) -> HeroDataModel {
        return self.model.listHeros[index]
    }
    
    func getListHeroes() -> [HeroDataModel] {
        return self.model.listHeros
    }
    
    func getCountListHeroes() -> Int {
        return self.model.listHeros.count
    }
}
