//
//  SavedPageView.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit
import SDWebImage

class SavedPageView: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    let modelView = SavedPageMV()
    let localData = LocalData()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    func initPageData() {
        collectionView?.register(UINib(nibName: "HeroCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeroCollectionViewCell")
        
        let layout : UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let w = Int(UIScreen.main.bounds.width)
        layout.itemSize = CGSize(width: (w/2) - 40, height: w/3)
        collectionView.collectionViewLayout = layout
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        modelView.loadListHeroesLocally()
        collectionView.reloadData()
    }
}

extension SavedPageView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.modelView.getCountListHeroes()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroCollectionViewCell", for: indexPath) as? HeroCollectionViewCell
        let model  = self.modelView.getHeroAtIndex(index: indexPath.row)
        
        cell?.lbName.text = model.name
        cell?.delegate = self
        cell?.setCurrentHeroModel(model: model)
        cell?.img.sd_setImage(with: URL(string: model.image?.url ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"), options: .allowInvalidSSLCertificates, completed: nil)
        return cell ?? HeroCollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 10, bottom: 5, right: 10)
    }
}

extension SavedPageView : HeroCollectionViewCellDelegate {
    func onSavedItemCellClick(heroModel: HeroDataModel) {
        self.localData.setSelectedHeroItem(hero: heroModel) // Set current Hero Selected
        MenuButtonView.selectedIndex.value = 2 // Redirec to Tab 2 -> InfoPage
    }
}
