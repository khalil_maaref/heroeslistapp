//
//  HeroDataResponse.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

struct HeroDataResponse: BasicResponse {
    var response: String?
    
    var error: String?
    
    let resultsFor : String?
    
    var results : [HeroDataModel]?
    
    enum CodingKeys: String, CodingKey {
        case response, results
        case resultsFor = "results-for"
    }
}
