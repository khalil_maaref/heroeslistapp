//
//  BasicResponse.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation


protocol BasicResponse: Decodable{
    var response: String? { get }
    var error: String? { get }
}


