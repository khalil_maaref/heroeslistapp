//
//  Api.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation
import Alamofire

class Api {
    
    func getJsonData(urlPath: String, method: HTTPMethod, params : [String:Any]?, headers: [String: String], encoding: ParameterEncoding, completion: @escaping ((Data?)-> Void))
    {
        Alamofire.request(urlPath, method: method, parameters: params, encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            if let data = response.data {
                completion(data)
            }
            else{
                print("No data found")
                completion(nil)
            }
        }
    }
    
    
    func getListHeroesApiByValue(valueToSearch: String = "a", completion: @escaping (([HeroDataModel])->Void)) {
        //let headers : [String: String] = ["Content-Type":"application/json"]
        let url = "\(CommonUtils.BaseURL)/search/\(valueToSearch)"
        getJsonData(urlPath: url, method: .get, params: nil, headers: [:], encoding: JSONEncoding.default) { (data) in
            guard data != nil else {
                completion([])
                return
            }
            let decoder = JSONDecoder()
            
            do{
                let res = try decoder.decode(HeroDataResponse.self, from: data!)
                if (res.response ?? "") == "success" {
                    print("Heroes List Api Succeed (count = \(res.results?.count ?? 0)")
                    completion(res.results ?? [])
                }
                else
                {
                    print("Heroes List Api Failed res = \(res.error ?? "")")
                    completion([])
                }
            }catch let err{
                print("error:  \(err)")
                completion([])
            }
            
            
        }
    }
    
}
