//
//  HeroResponseModel.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation

struct HeroDataModel : Codable {
    let id, name: String?
    let powerstats: PowerstatsInfo?
    let biography: BiographyInfo?
    let appearance: AppearanceInfo?
    let work: WorkInfo?
    let connections: ConnectionsInfo?
    let image: ImageInfo?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
        self.powerstats = nil
        self.biography = nil
        self.appearance = nil
        self.work = nil
        self.connections = nil
        self.image = nil
    }
}

// MARK: - Appearance
struct AppearanceInfo: Codable {
    let gender, race: String?
    let height, weight: [String]?
    let eyeColor, hairColor: String?

    enum CodingKeys: String, CodingKey {
        case gender, race, height, weight
        case eyeColor = "eye-color"
        case hairColor = "hair-color"
    }
}

// MARK: - Biography
struct BiographyInfo: Codable {
    let fullName, alterEgos: String?
    let aliases: [String]?
    let placeOfBirth, firstAppearance, publisher, alignment: String?

    enum CodingKeys: String, CodingKey {
        case fullName = "full-name"
        case alterEgos = "alter-egos"
        case aliases
        case placeOfBirth = "place-of-birth"
        case firstAppearance = "first-appearance"
        case publisher, alignment
    }
}

// MARK: - Connections
struct ConnectionsInfo: Codable {
    let groupAffiliation, relatives: String?

    enum CodingKeys: String, CodingKey {
        case groupAffiliation = "group-affiliation"
        case relatives
    }
}

// MARK: - Image
struct ImageInfo: Codable {
    let url: String?
}

// MARK: - Powerstats
struct PowerstatsInfo: Codable {
    let intelligence, strength, speed, durability: String?
    let power, combat: String?
}

// MARK: - Work
struct WorkInfo: Codable {
    let occupation, base: String?
}
