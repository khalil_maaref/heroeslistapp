//
//  LocalData.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 26/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation


class LocalData {
    
    private var CURRENT_SELECTED_ITEM = "CURRENT_SELECTED_ITEM"
    private var LIST_VIEWED_ITEMS =  "LIST_VIEWED_ITEMS"
    
    
    
    func setSelectedHeroItem(hero: HeroDataModel) {
        self.saveEncodedData(type: .heroData, key: CURRENT_SELECTED_ITEM, value: hero)
    }
    
    func getSelectedHeroItem() -> HeroDataModel {
        if let model = getDecodedData(type: .heroData, key: CURRENT_SELECTED_ITEM) as? HeroDataModel {
            return model
        }
        return HeroDataModel(id: "-1", name: "-")
    }
    
    func setListViwedHeroes(list: [HeroDataModel]) {
        self.saveEncodedData(type: .listHeroesData, key: LIST_VIEWED_ITEMS, value: list)
    }
    
    func appendItemHeroToViewedHeroes(item: HeroDataModel) {
        guard item.id != nil && !(item.id?.contains("-1") ?? true) else {
            return
        }
        if var list = getDecodedData(type: .listHeroesData, key: LIST_VIEWED_ITEMS) as? [HeroDataModel] {
            list.append(item)
            self.saveEncodedData(type: .listHeroesData, key: LIST_VIEWED_ITEMS, value: list)
            
        } else {
            var arr : [HeroDataModel] = []
            arr.append(item)
            self.saveEncodedData(type: .listHeroesData, key: LIST_VIEWED_ITEMS, value: arr)
        }
    }
    func getListViwedHeroes() -> [HeroDataModel] {
        if let list = getDecodedData(type: .listHeroesData, key: LIST_VIEWED_ITEMS) as? [HeroDataModel] {
            return list
        }
        return []
    }
    
    func saveEncodedData(type: TypeDataElement, key: String, value: Any) {
        switch type {
        case .heroData:
            if let data = value as? HeroDataModel {
                let jsonData = try! JSONEncoder().encode(data)
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    UserDefaults.standard.set(jsonString, forKey: key)
                }
            }
            
        case .listHeroesData:
            if let data = value as? [HeroDataModel] {
                let jsonData = try! JSONEncoder().encode(data)
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    UserDefaults.standard.set(jsonString, forKey: key)
                }
            }
        }
        
    }
    
    func getDecodedData(type: TypeDataElement, key: String) -> Any? {
        let strData = UserDefaults.standard.string(forKey: key) ?? ""
        let json = strData.data(using: .utf8)
        guard json != nil else { return nil }
        do {
            switch type {
            case .heroData:
                return try JSONDecoder().decode(HeroDataModel.self, from: json!)
            case .listHeroesData:
                return try JSONDecoder().decode([HeroDataModel].self, from: json!)
            }
        } catch{
            print("Error while decoding Json")
        }
        return nil
    }
}

enum TypeDataElement {
    case heroData
    case listHeroesData
}
