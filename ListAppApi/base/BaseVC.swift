//
//  BaseVC.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import UIKit
import JGProgressHUD

protocol HudLoadingDelegate {
    func showLoading()
    func hideLoading()
}

class BaseVC: UIViewController {
    
    var hud = JGProgressHUD(style: .dark)
    var cancelKeyboard : Bool = false
    var tap: UITapGestureRecognizer!
    var enabledTap = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if enabledTap {
            let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyBoard(tap:)))
            self.view.isUserInteractionEnabled = true
            self.view.addGestureRecognizer(tap)
        }
    }
    
    func disableViewClickGesture(){
        enabledTap = false
    }
    
    func showHudLoading(hideKey : Bool = true, completion: ((Bool)->())? = nil) {
        if hideKey {
            hideKeyboard()
        }
        hud.textLabel.text = NSLocalizedString("Loading..", comment: "")
        let vw = (self.view)!
        hud.indicatorView?.contentView?.tintColor = #colorLiteral(red: 0, green: 0.8710491061, blue: 0.737411797, alpha: 1)
        hud.show(in: vw)
        guard completion != nil else {
            return
        }
        completion!(true)
    }
    
    @objc func closeKeyBoard(tap: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func hideHudLoading(){
        hud.dismiss(animated: true)
    }
    
    func showingKeyboard() {
        self.cancelKeyboard = true
        //self.view.layoutMargins.bottom = 100
    }
    
    func hideKeyboard() {
        //self.cancelKeyboard = true
        self.view.endEditing(true)
        //self.view.layoutMargins.bottom = 0
    }
    
    func hideErrorMessage() {
        _ = self.view.subviews.map { (subView) in
            if (subView.tag == 100 || subView.tag == 101) {
                subView.removeFromSuperview()
            }
        }
    }
    
    func onModalManualPressed() {
        // todo: on SavePressed.
        hideErrorMessage()
    }
    
    func onModalRetryPressed() {
        // todo: on RetryPressed.
        hideErrorMessage()
    }
    
    func onModalOkPressed() {
        hideErrorMessage()
    }
}
