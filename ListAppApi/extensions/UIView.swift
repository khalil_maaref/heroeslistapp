//
//  UIView.swift
//  ListAppApi
//
//  Created by Khalil Maaref on 25/11/2019.
//  Copyright © 2019 Khalil Maaref. All rights reserved.
//

import Foundation
import UIKit

enum ViewPostion{
    case margin
    
    case left
    case right
    case top
    case bottom
    case center
    
    case topLeft
    case topRight
    
    case bottomLeft
    case bottomRight
    
    case under
}

extension UIView {
        
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func clearSubViews() -> Void {
        for v in self.subviews{
            v.removeFromSuperview()
        }
    }
    
    func removeSubViewWithTag(view: UIView? = nil, tag: Int) {
        _ = self.subviews.map { (value) in
            if value.tag == tag {
                value.removeFromSuperview()
            }
        }
    }
    
    func fixInView(_ container: UIView) -> Void {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.frame = container.frame
        container.addSubview(self)
        
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
    
    func fixConstraintsToView(_ container: UIView, positionType: ViewPostion, top: CGFloat?, right: CGFloat?, bottom: CGFloat?, left: CGFloat?) -> Void {
        self.translatesAutoresizingMaskIntoConstraints = false
        let defaultValue : CGFloat = 20
        
        switch positionType {
        case .margin:
            if top != nil{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: top!).isActive = true
            }
            if right != nil{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: right! * -1).isActive = true
            }
            if bottom != nil{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: bottom! * -1).isActive = true
            }
            if left != nil{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: left!).isActive = true
            }
        case .center:
            NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1.0, constant: 0).isActive = true
            
            NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
            
        case .bottom:
            if bottom != nil{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: bottom! * -1).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: defaultValue * -1).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
            }
        case .top:
            if top != nil{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: top!).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: defaultValue).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
            }
        case .left:
            if left != nil{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: left!).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: defaultValue).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
            }
            
            if right != nil{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: right! * -1).isActive = true
            }
            
            if top == nil && bottom == nil {
                NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1.0, constant: 0).isActive = true
            }
            
            if top != nil{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: container, attribute: .top, multiplier: 1.0, constant: top!).isActive = true
            }
            
            if bottom != nil{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: container, attribute: .bottom, multiplier: 1.0, constant: bottom! * -1).isActive = true
            }
            
        case .right:
            if right != nil{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: right! * -1).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: defaultValue * -1).isActive = true
                
                NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
            }
            
            if left != nil{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: left!).isActive = true
            }
            if top != nil{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: top!).isActive = true
            }
            if bottom != nil{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: bottom! * -1).isActive = true
            }
            
        case .topRight: // Mising Setup
            if right != nil{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: right! * -1).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: defaultValue * -1).isActive = true
            }
            
            if left != nil {
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: left!).isActive = true
            }
            else {
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: defaultValue).isActive = true
            }
            if top != nil{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: top!).isActive = true
            }else{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: defaultValue).isActive = true
            }
            
            
        case .topLeft: // Mising Setup
            if left != nil{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: left!).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: defaultValue).isActive = true
            }
            
            if right != nil {
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: right! * -1).isActive = true
            }
            else {
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: defaultValue * -1).isActive = true
            }
            
            if top != nil{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: top!).isActive = true
            }else{
                NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: defaultValue).isActive = true
            }
            
        case .bottomLeft: // Mising Setup
            if left != nil{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: left!).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: defaultValue).isActive = true
            }
            
            if right != nil {
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: right! * -1).isActive = true
            }
            else {
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: defaultValue * -1).isActive = true
            }
            
            if bottom != nil{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: bottom! * -1).isActive = true
            }else{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: defaultValue * -1).isActive = true
            }
            
            
            
        case .bottomRight: // Mising Setup
            if right != nil{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: right! * -1).isActive = true
            }
            else{
                NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: defaultValue * -1).isActive = true
            }
            
            if left != nil {
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: left!).isActive = true
            }
            else {
                NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: defaultValue).isActive = true
            }
            
            if bottom != nil{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: bottom! * -1).isActive = true
            }else{
                NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: defaultValue * -1).isActive = true
            }
        case .under:
            NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: top ?? 0).isActive = true
        }
    }
}
